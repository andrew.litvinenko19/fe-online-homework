package tests;

import happyFamily.Family;
import happyFamily.Human;
import happyFamily.Pet;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class FamilyTest {

    private Family family;

    @Test
    public void testSuccessDeleteFromArray(){
        Human human = new Human("TestIvan", "TestIvanov", 1900, 288, new String[][]{});
        Human human2 = new Human("TestLida", "TestIvanova", 1920, 28, new String[][]{});
        Human child = new Human("sda", "sad2", 1220, 218, new String[][]{});
        Pet pet = new Pet(Pet.Species.CAT, "Мурзик", new String[]{}, 13, 100);
        family = new Family(human, human2, pet);
        family.addChild(child);
        boolean actual =  family.deleteChild(0);
        Assertions.assertTrue(actual);

    }

    @Test
    public void testFailDeleteFromArray(){
        Human human = new Human("TestIvan", "TestIvanov", 1900, 288, new String[][]{});
        Human human2 = new Human("TestLida", "TestIvanova", 1920, 28, new String[][]{});
        Human child = new Human("sda", "sad2", 1220, 218, new String[][]{});
        Pet pet = new Pet(Pet.Species.CAT, "Мурзик", new String[]{}, 13, 100);
        family = new Family(human, human2, pet);
        family.addChild(child);
        Human[] expected = family.getChildren();
        boolean actual =  family.deleteChild(900);
        Assertions.assertFalse(actual);
        Assertions.assertArrayEquals(expected, family.getChildren());
    }

    @Test
    public void testAddToArray(){
        Human human = new Human("TestIvan", "TestIvanov", 1900, 288, new String[][]{});
        Human human2 = new Human("TestLida", "TestIvanova", 1920, 28, new String[][]{});
        Human child = new Human("sda", "sad2", 1220, 218, new String[][]{});
        Pet pet = new Pet(Pet.Species.CAT, "Мурзик", new String[]{}, 13, 100);
        family = new Family(human, human2, pet);

        int expected = family.getChildren().length + 1;
        family.addChild(child);
        int actual = family.getChildren().length;
        Assertions.assertEquals(expected, actual);
        Assertions.assertEquals(child, family.getChildren()[actual-1]);
    }

    @Test
    public void testSuccessCountOfFamily(){
        Human human = new Human("TestIvan", "TestIvanov", 1900, 288, new String[][]{});
        Human human2 = new Human("TestLida", "TestIvanova", 1920, 28, new String[][]{});
        Human child = new Human("sda", "sad2", 1220, 218, new String[][]{});
        Pet pet = new Pet(Pet.Species.CAT, "Мурзик", new String[]{}, 13, 100);
        family = new Family(human, human2, pet);
        family.addChild(child);
        int expected = 2;

        int actual =  family.countFamily();
        Assertions.assertEquals(expected,actual);
    }

}
