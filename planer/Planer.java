package planer;

import java.util.Objects;
import java.util.Scanner;

public class Planer {

    public static String findSchedule(String[][] sch, String day) {
        for (int i = 0; i < sch.length; i++) {
            for (int j = 0; j < sch[i].length; j++) {
                if (Objects.equals(sch[i][j], day)) {
                    return sch[i][j + 1];
                }
            }
        }
        return "";
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String day;
        String[][] schedule = new String[][]{
                {"monday", "go to courses; watch a film"},
                {"tuesday", ""},
                {"wednesday", "read book"},
                {"thursday", ""},
                {"friday", "play in games with friends"},
                {"saturday", ""},
                {"sunday", "do home work"},
        };

        while (true) {
            System.out.println("Please, input the day of the week:");
            day = scanner.next();

            switch (day.toLowerCase()) {
                case "monday":
                    System.out.println("Your tasks for Monday: " + findSchedule(schedule, day.toLowerCase()));
                    break;
                case "tuesday":
                    System.out.println("Your tasks for Tuesday: " + findSchedule(schedule, day.toLowerCase()));
                    break;
                case "wednesday":
                    System.out.println("Your tasks for Wednesday: " + findSchedule(schedule, day.toLowerCase()));
                    break;
                case "thursday":
                    System.out.println("Your tasks for Thursday: " + findSchedule(schedule, day.toLowerCase()));
                    break;
                case "friday":
                    System.out.println("Your tasks for Friday: " + findSchedule(schedule, day.toLowerCase()));
                    break;
                case "saturday":
                    System.out.println("Your tasks for Saturday: " + findSchedule(schedule, day.toLowerCase()));
                    break;
                case "sunday":
                    System.out.println("Your tasks for Sunday: " + findSchedule(schedule, day.toLowerCase()));
                    break;
                case "exit":
                    System.exit(0);
                default:
                    System.out.println("Sorry, I don't understand you, please try again.");
            }
        }

    }
}

