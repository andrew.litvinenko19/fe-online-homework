package happyFamily;

import java.util.Arrays;

public class Pet {
    public enum Species {
        DOG,
        CAT,
        PARROT,
        FISH
    }

    private Species species;
    private String nickname;
    private String[] habits;
    private int age;
    private int trickLevel;

    public Species getSpecies() {
        return species;
    }

    public void setSpecies(Species species) {
        this.species = species;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String[] getHabits() {
        return habits;
    }

    public void setHabits(String[] habits) {
        this.habits = habits;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

//    public Pet(String species, String nickname) {
//        this.species = species;
//        this.nickname = nickname;
//    }

    public Pet(Species species, String nickname, String[] habits, int age, int trickLevel) {
        this.species = species;
        this.nickname = nickname;
        this.habits = habits;
        this.age = age;
        this.trickLevel = Math.max(age, 100);
        ;
    }

    @Override
    public String toString() {
        return this.species.name() + '{' +
                ", nickname='" + nickname + '\'' +
                ", age=" + age +
                ", trickLevel=" + trickLevel +
                ", habits=" + Arrays.toString(habits) +
                '}';
    }

    protected void finalize() throws Throwable {
        super.finalize();
        System.out.println(this.toString());
    }

    public void eat() {
        System.out.println("Я кушаю!");
    }

    public void respond() {
        System.out.println(" Привет, хозяин. Я - " + this.nickname + ". Я соскучился!");
    }

    public void foul() {
        System.out.println("Нужно хорошо замести следы...");
    }


}
