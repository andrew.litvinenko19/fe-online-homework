package happyFamily;

import java.util.Arrays;

import static happyFamily.Pet.Species;
import static happyFamily.Human.DayOfWeek;

public class Main {
    public static void main(String[] args) {
        String[][] sch = new String[][]{
                {DayOfWeek.MONDAY.name(), "to-do1"},
                {DayOfWeek.TUESDAY.name(), "to-do2"},
                {DayOfWeek.THURSDAY.name(), "to-do3"},
                {DayOfWeek.WEDNESDAY.name(), "to-do4"},
                {DayOfWeek.FRIDAY.name(), "to-do5"},
                {DayOfWeek.SATURDAY.name(), "to-do6"},
                {DayOfWeek.SUNDAY.name(), "to-do7"},
        };

        String[] dogHabits = new String[]{"habit-1", "habit-2", "habit-3"};
        Pet sharick = new Pet(Species.DOG, "Шарик", dogHabits, 3, 78);
        Human eva = new Human("Eva", "Green", 1987, 90, sch);
        Human ivan = new Human("Ivan", "Petrov", 1988, 23, sch);
        Human child = new Human("Semen", "Petrov", 2000, 43, sch);
        Human child1 = new Human("Anton", "Petrov", 2010, 3, sch);
        Human child2 = new Human("Elena", "Petrova", 2009, 23, sch);
        Family family = new Family(eva, ivan, sharick);
        family.addChild(child);
        family.addChild(child1);
        family.addChild(child2);
        family.getChildren();
        System.out.println(family.toString());

        ivan.describePet();

        System.out.println(family.deleteChild(0));
        System.out.println(Arrays.toString(family.getChildren()));
        Human olha = new Human("Olha", "Brawn", 2001, 190, sch);
        System.out.println(family.deleteChild(1));

        System.out.println(Arrays.toString(family.getChildren()));
        Pet murzik = new Pet(Species.CAT, "Мурзик", dogHabits, 13, 100);
        Family family1 = new Family(child1, olha, murzik);
        family1.countFamily();
        olha.greetPet();
        murzik.eat();
        murzik.foul();

//        for (int i = 0; i < 900000000; i++) {
//            int rand = (int) Math.floor(Math.random() * 2000);
//            Human obj =  new Human("Olha", "Brawn",rand, 190, sch);
//        }
//        System.gc();

    }
}
