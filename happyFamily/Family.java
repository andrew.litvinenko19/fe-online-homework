package happyFamily;

import java.util.Arrays;
import java.util.Objects;

public class Family {
    private Human mother;
    private Human father;
    private Human[] children;
    private Pet pet;

    public Family(Human mother, Human father, Pet pet) {
        this.mother = mother;
        this.father = father;
        this.pet = pet;
        this.children = new Human[]{};
        this.mother.setFamily(this);
        this.father.setFamily(this);
    }

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public Human[] getChildren() {
        return children;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public void addChild(Human child) {
        child.setFamily(this);
        System.out.println(this.children.length);
        Human[] newChildrens = Arrays.copyOf(this.children, this.children.length + 1);
        newChildrens[newChildrens.length - 1] = child;
        this.children = newChildrens;
    }

    public boolean deleteChild(int index) {
        Human[] newArr = new Human[this.children.length - 1];

        if (index < 0) return false;
        else if (index > this.children.length) return false;

        if (index > 0) {
            System.arraycopy(this.children, 0, newArr, 0, index);
            System.arraycopy(this.children, index + 1, newArr, index, this.children.length - index - 1);
            this.children = newArr;
            return true;
        } else if (index == 0) {
            System.arraycopy(this.children, index + 1, newArr, index, this.children.length - index - 1);
            this.children = newArr;
            return true;
        }
        return false;
    }

    public int countFamily() {
        if (this.father != null && this.mother != null) {
            return 2;
        } else return 0;
    }

    protected void finalize() throws Throwable {
        super.finalize();
        System.out.println(this.toString());
    }

    @Override
    public String toString() {
        return "Family{" +
                "mother=" + mother +
                ", father=" + father +
                ", children=" + Arrays.toString(children) +
                ", pet=" + pet +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return Objects.equals(mother, family.mother) && Objects.equals(father, family.father) && Arrays.equals(children, family.children) && Objects.equals(pet, family.pet);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mother, father, Arrays.hashCode(children), pet);
    }
}
