package numbers;

import java.util.Random;
import java.util.Scanner;

public class Numbers {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String stop = "no";
        String name;
        Random random = new Random();

        int randNum = random.nextInt(100);
        int userNum;

        System.out.println("input your name!");
        name = scanner.nextLine();
        System.out.println(name);
        System.out.println("Let the game begin!");
        System.out.println("Input your number: ");
        userNum = scanner.nextInt();

        while (stop.equals("no")) {
            if (userNum < randNum) {
                System.out.println("Your number is too small. Please, try again.");
                userNum = scanner.nextInt();
            } else if (userNum > randNum) {
                System.out.println("Your number is too big. Please, try again.");
                userNum = scanner.nextInt();
            } else {
                System.out.println("Congratulations " + name + "!");
                stop = "yes";
            }
        }
    }

}


