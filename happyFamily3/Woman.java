package happyFamily3;

public final class Woman extends Human{
    public Woman(String name, String surname, int year, int iq, String[][] schedule) {
        super(name, surname, year, iq, schedule);
    }

    public void greetPet(){
        System.out.println("Привет, я твоя хозяйка,  " + super.getFamily().getPet().getNickname());
    }

    public void makeup(){
        System.out.println("Красится, по-женски!");
    }
}
