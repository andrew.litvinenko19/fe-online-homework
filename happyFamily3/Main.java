package happyFamily3;


import happyFamily3.abstractClass.Pet;
import happyFamily3.abstractClass.extended.Dog;
import happyFamily3.abstractClass.extended.DomesticCat;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        String[][] sch = new String[][]{
                {Human.DayOfWeek.MONDAY.name(), "to-do1"},
                {Human.DayOfWeek.TUESDAY.name(), "to-do2"},
                {Human.DayOfWeek.THURSDAY.name(), "to-do3"},
                {Human.DayOfWeek.WEDNESDAY.name(), "to-do4"},
                {Human.DayOfWeek.FRIDAY.name(), "to-do5"},
                {Human.DayOfWeek.SATURDAY.name(), "to-do6"},
                {Human.DayOfWeek.SUNDAY.name(), "to-do7"},
        };

        String[] dogHabits = new String[]{"habit-1", "habit-2", "habit-3"};
        Dog sharick = new Dog(Pet.Species.DOG, "Шарик", dogHabits, 3, 78);
        Woman eva = new Woman("Eva", "Green", 1987, 90, sch);
        Man ivan = new Man("Ivan", "Petrov", 1988, 23, sch);
        Man child = new Man("Semen", "Petrov", 2000, 43, sch);
        Man child1 = new Man("Anton", "Petrov", 2010, 3, sch);
        Woman child2 = new Woman("Elena", "Petrova", 2009, 23, sch);
        Family family = new Family(eva, ivan, sharick);

        family.addChild(child);
        family.addChild(child1);
        family.addChild(child2);
        family.getChildren();
        System.out.println(family.toString());

        ivan.describePet();

        System.out.println(family.deleteChild(0));
        System.out.println(Arrays.toString(family.getChildren()));
        Woman olha = new Woman("Olha", "Brawn", 2001, 190, sch);
        System.out.println(family.deleteChild(1));

        System.out.println(Arrays.toString(family.getChildren()));

        DomesticCat murzik = new DomesticCat(Pet.Species.DOMESTICAT, "Мурзик", dogHabits, 13, 100);
        Family family1 = new Family(child1, olha, murzik);
        family1.countFamily();
        olha.greetPet();
        murzik.eat();
        murzik.foul();

    }
}
