package happyFamily3.abstractClass;

import java.util.Arrays;

public abstract class Pet {
    public enum Species {
        DOG,
        DOMESTICAT,
        ROBOCAT,
        FISH,
        UNKNOWN
    }
    private Species species;
    private String nickname;
    private String[] habits;
    private int age;
    private int trickLevel;

    public Species getSpecies() {return species;}
    public String getNickname() {return nickname;}
    public int getAge() {return age;}
    public int getTrickLevel() {return trickLevel;}

    public Pet(Species species, String nickname, String[] habits, int age, int trickLevel) {
        this.species = species;
        this.nickname = nickname;
        this.habits = habits;
        this.age = age;
        this.trickLevel = Math.max(trickLevel, 100);;
    }
    @Override
    public String toString() {
        return species.toString() + '{' +
                ", nickname='" + nickname + '\'' +
                ", age=" + age +
                ", trickLevel=" + trickLevel +
                ", habits=" + Arrays.toString(habits) +
                '}';
    }
    protected void finalize() throws Throwable {
        super.finalize();
        System.out.println(this.toString());
    }
    public void eat(){System.out.println("Я кушаю!");};
    public abstract void respond();
}
