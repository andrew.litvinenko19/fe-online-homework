package happyFamily3.abstractClass.extended;

import happyFamily3.abstractClass.Foulable;
import happyFamily3.abstractClass.Pet;

public class DomesticCat extends Pet implements Foulable {

    public DomesticCat(Species species, String nickname, String[] habits, int age, int trickLevel){
        super(Species.valueOf(String.valueOf(species)), nickname, habits,  age, Math.max(trickLevel, 100));
    }

    @Override
    public void respond() {
        System.out.println("Мяу, я пушистый кот по кличке " + super.getNickname() + " обычно я не рассказываю о себе ничего но, мне нравится делать: "
                +  super.getSpecies() + ". Эти люди живут со мной уже "
                + super.getAge() + "лет.");
    }

    @Override
    public void foul() {
        System.out.println("о да, класс, я ободрал обои, пойду поем!");
    }
}
