package happyFamily3.abstractClass.extended;

import happyFamily3.abstractClass.Pet;

public class Fish extends Pet {

    public Fish(Species species, String nickname, String[] habits, int age, int trickLevel){
        super(Species.valueOf(String.valueOf(species)), nickname, habits,  age, Math.max(trickLevel, 100));
    }

    @Override
    public void respond() {
        System.out.println("Привет, я рыбка по имени " + super.getNickname() + " я плаваю в аквариуме, мне нравится делать: "
                +  super.getSpecies() + ". Мне уже "
                + super.getAge() + "лет.");
    }


}
