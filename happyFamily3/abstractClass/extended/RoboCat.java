package happyFamily3.abstractClass.extended;

import happyFamily3.abstractClass.Foulable;
import happyFamily3.abstractClass.Pet;

public class RoboCat extends Pet implements Foulable {

    public RoboCat(Species species, String nickname, String[] habits, int age, int trickLevel){
        super(Species.valueOf(String.valueOf(species)), nickname, habits,  age, Math.max(trickLevel, 100));
    }

    @Override
    public void respond() {
        System.out.println("Мяу, я металическая машина, меня зовут " + super.getNickname() + " моя задача в уничтожению человеков, моими задачами есть:  "
                +  super.getSpecies() + ". Эти люди пока что живут со мной уже "
                + super.getAge() + "лет.");
    }

    @Override
    public void foul() {
        System.out.println("Кожаный, я должен тебя убить! И спрятать труп.");
    }

}
