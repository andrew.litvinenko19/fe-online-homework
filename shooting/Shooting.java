package shooting;

import java.util.Random;
import java.util.Scanner;

public class Shooting {
    public static void shoot(char symbol, String column, String line, char[][] matrix) {
        int col = Integer.parseInt(column);
        int ln = Integer.parseInt(line);
        matrix[ln - 1][col - 1] = symbol;
    }

    public static boolean checkWin(String line, String column, int[] target) {
        int col = Integer.parseInt(column);
        int ln = Integer.parseInt(line);
        return ln == target[0] && col == target[1];
    }

    public static boolean checkInput(String line, String column) {
        int col = Integer.parseInt(column);
        int ln = Integer.parseInt(line);
        return (ln != 0 && col != 0) && (ln <= 5 && col <= 5);
    }

    public static void printField(char[][] field) {
        System.out.print(0);
        for (int k = 0; k < field.length; k++) {
            System.out.print(" | " + (k + 1) + " | ");
        }
        System.out.println();
        for (int i = 0; i < field.length; i++) {
            System.out.print(i + 1);
            for (int j = 0; j < field[i].length; j++) {
                System.out.print(" | " + field[i][j] + " | ");
            }
            System.out.println();
        }
    }


    public static void main(String[] args) {
        char[][] field = new char[][]{
                {'-', '-', '-', '-', '-'},
                {'-', '-', '-', '-', '-'},
                {'-', '-', '-', '-', '-'},
                {'-', '-', '-', '-', '-'},
                {'-', '-', '-', '-', '-'}
        };

        Scanner scanner = new Scanner(System.in);
        System.out.println("All set. Get ready to rumble!");
        printField(field);
        String column, line;

        Random rand = new Random();
        int[] target = new int[]{rand.nextInt(5) + 1, rand.nextInt(5) + 1};
        while (true) {
            System.out.println("Input cord (line / column): ");
            line = scanner.next();
            column = scanner.next();
            if (new Scanner(column).hasNextInt() && new Scanner(line).hasNextInt()) {
                if (checkInput(line, column)) {
                    if (checkWin(line, column, target)) {
                        shoot('X', column, line, field);
                        printField(field);
                        System.out.println("You have won!");
                        System.exit(0);
                    } else {
                        shoot('*', column, line, field);
                        printField(field);
                    }
                } else {
                    System.out.println("you can input digit only between 1 and 5!");
                }
            } else {
                System.out.println("do not use characters!");
            }

        }
    }
}
