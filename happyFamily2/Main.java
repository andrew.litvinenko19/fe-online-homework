package happyFamily2;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        String[][] sch = new String[][]{
                {"1", "to-do1"},
                {"2", "to-do2"},
                {"3", "to-do3"},
        };
        String[] dogHabits = new String[]{"habit-1", "habit-2", "habit-3"};
        happyFamily2.Pet sharick = new happyFamily2.Pet("dog", "sharick",  dogHabits, 3, 78);
        happyFamily2.Human eva = new happyFamily2.Human("Eva", "Green", 1987, 90, sch);
        happyFamily2.Human ivan = new happyFamily2.Human("Ivan", "Petrov", 1988, 23, sch);
        happyFamily2.Human child = new happyFamily2.Human("Semen", "Petrov", 2000, 43, sch);
        happyFamily2.Human child1 = new happyFamily2.Human("Anton", "Petrov", 2010, 3, sch);
        happyFamily2.Human child2 = new happyFamily2.Human("Elena", "Petrova", 2009, 23, sch);
        happyFamily2.Family family = new happyFamily2.Family(eva, ivan, sharick);
        family.addChild(child);
        family.addChild(child1);
        family.addChild(child2);
        family.getChildren();
        System.out.println(family.toString());

        ivan.describePet();

        System.out.println(family.deleteChild(0));
        System.out.println(Arrays.toString(family.getChildren()));
        happyFamily2.Human olha =  new happyFamily2.Human("Olha", "Brawn", 2001, 190, sch);
        System.out.println(family.deleteChild(1));

        System.out.println(Arrays.toString(family.getChildren()));
        happyFamily2.Pet murzik = new happyFamily2.Pet("cat", "murzik",  dogHabits, 13, 100);
        happyFamily2.Family family1 = new happyFamily2.Family(child1, olha, murzik);
        family1.countFamily();
        olha.greetPet();
        murzik.eat();
        murzik.foul();

    }

}
