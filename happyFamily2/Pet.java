package happyFamily2;

import java.util.Arrays;

public class Pet {
    private String species;
    private String nickname;
    private String[] habits;
    private int age;
    private int trickLevel;

    public String getSpecies() {
        return species;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String[] getHabits() {
        return habits;
    }

    public void setHabits(String[] habits) {
        this.habits = habits;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

//    public Pet(String species, String nickname) {
//        this.species = species;
//        this.nickname = nickname;
//    }

    public Pet(String species, String nickname, String[] habits, int age, int trickLevel) {
        this.species = species;
        this.nickname = nickname;
        this.habits = habits;
        this.age = age;
        this.trickLevel = Math.max(age, 100);;
    }

    @Override
    public String toString() {
        return species + '{' +
                ", nickname='" + nickname + '\'' +
                ", age=" + age +
                ", trickLevel=" + trickLevel +
                ", habits=" + Arrays.toString(habits) +
                '}';
    }

    public void eat() {
        System.out.println("Я кушаю!");
    }

    public void respond() {
        System.out.println(" Привет, хозяин. Я - " + this.nickname + ". Я соскучился!");
    }

    public void foul() {
        System.out.println("Нужно хорошо замести следы...");
    }



}
